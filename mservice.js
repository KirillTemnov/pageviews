import {promisify} from 'util';
import {createClient} from 'redis';

function currentMinute() {
  const coeff = 1000 * 60;
  const dtime = (new Date).getTime();
  return new Date(Math.round(dtime / coeff) * coeff).getTime();
}

export function pageView() {
  const client = createClient(process.env.REDIS_CONNECTION);
  const currentMin = currentMinute();
  const getAsync = promisify(client.get).bind(client);

  return getAsync(currentMin).then(res => {
    let pageviews = (parseInt(res, 10) || 0) + 1;
    client.set(currentMin, pageviews);
    client.quit();
    return pageviews;
  });
}

export function pageViewsThisMinute() {
  const client = createClient(process.env.REDIS_CONNECTION);
  const getAsync = promisify(client.get).bind(client);
  const currentMin = currentMinute();
  return getAsync(currentMin).then(res => {
    client.quit();
    return res;
  });
}
