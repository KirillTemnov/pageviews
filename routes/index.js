const express = require('express');
let router = express.Router();
import {pageView, pageViewsThisMinute} from '../mservice';
const jsonError = {error: 'Internal error'};

/* GET home page. */
router.get('/', (req, res, next) => {
  pageView();
  pageViewsThisMinute().then(
    result => {
      res.render('index', { title: 'Express', pageViews: result});
    },
    error => {
      // TODO show error page
      res.status(503)
        .render('index', { title: 'Express / error', pageViews: error});
    }
  );
});

/* health status */
router.get('/api/health', (req, res, next) => {
  pageViewsThisMinute().then(
    result => { res.json({status: 'ok'}); },
    fail => { res.status(503).json(jsonError); }
  );
});

/* send new pageview */
router.post('/api/pageview', (req, res, next) => {
  pageView().then(
    result => { res.status(201).json({pageviews: result}); },
    fail => { res.status(503).json(jsonError); }
  );
});

/* get statistics for pageviews for last minute */
router.get('/api/stat', (req, res, next) => {
  pageViewsThisMinute().then(
    result => { res.json({pageviews: result || 0}); },
    fail => { res.status(503).json(jsonError); }
  );
});

module.exports = router;
