require('babel-register')({presets: [ 'env' ]});

const request = require('supertest');
const test = require('tape');
const app = require('../app');

test('GET /', (t) => {
  request(app)
    .get('/')
    .expect('Content-Type', /text\/html/)
    .expect(200)
    .end((err, res) => {
      if (err) throw err;

      t.ok(res.text.indexOf('Page views') > 0,
        'Body not containg "Page views" text');
      t.end();
    });
});

test('GET /api/health', (t) => {
  request(app)
    .get('/api/health')
    .expect('Content-Type', /application\/json/)
    .expect(200)
    .end((err, res) => {
      if (err) throw err;

      t.deepEqual(JSON.parse(res.res.text), {status: 'ok'},
        'Health status not ok');
      t.end();
    });
});

test('GET /api/stat', (t) => {
  request(app)
    .get('/api/stat')
    .expect('Content-Type', /application\/json/)
    .expect(200)
    .end((err, res) => {
      if (err) throw err;

      const pageviews = parseInt(JSON.parse(res.res.text).pageviews, 10);

      t.ok(pageviews >= 0, 'Stat response not match');
      t.end();
    });
});

test('POST /pageview increment page views', (t) => {
  request(app)
    .get('/api/stat')
    .expect('Content-Type', /application\/json/)
    .expect(200)
    .end((err, res) => {
      if (err) throw err;

      const pageviews = parseInt(JSON.parse(res.res.text).pageviews, 10);

      request(app)
        .post('/api/pageview')
        .expect('Content-Type', /application\/json/)
        .expect(201)
        .end((err, res) => {
          if (err) throw err;

          const secodPageviews =
            parseInt(JSON.parse(res.res.text).pageviews, 10);

          t.equal(pageviews + 1, secodPageviews, 'Page views not match');
          t.end();
        });
    });
});
