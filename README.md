# This is a test task for Driveback

1) Implement a NodeJS application, which shows the number of pageviews for the past 1 minute.
2) Implement transpiling of ES6 code to run on any Node version, starting from 6. Important: keep you build as tiny as possible for each version, depending on which ES6 features are supported by current Node version.
3) Implement dockerization of the application. Keep image size as small as possible so that final image doesn’t contain any dev dependencies or not transpiled sources (tip: https://docs.docker.com/engine/userguide/eng-image/multistage-build/#use-multi-stage-builds)
4) Build and run locally.
5) Make your application production-ready.
6) Deploy your docker image to AWS, Google Cloud or MS Azure as a load-balanced, horizontally scalable micro-service.
7) Implement CI pipeline, which automatically redeploys test application after git push to the master branch.
Prepare instructions in README.md file about how to deploy your app to production environment for other developers to reproduce.

## What was done

### 1. Node js application

#### Requirements
  - docker v18.0+
  - docker-compose 1.19+

#### Install application:

```bash
git clone git@bitbucket.org:KirillTemnov/pageviews.git
cd pageviews && docker-compose build app
```

#### Run application

```bash
docker-compose up app
```

Application running on 3000 port by default and has a few urls:

 - `GET /` - show page views for current munute and count visit as new page view
 - `GET /api/health` - show health status in json
 - `GET /api/stat` - show statistics in json without incrementing views
 - `POST /api/pageview` - create new page view

#### Run application tests

Integration tests:

```bash
docker-compose up app_test
```

Load tests:

```bash
ab -n 10000 -c 10 -k http://127.0.0.1:3000/
```

### 2. Transpiling of ES6 code to run on any Node version, starting from 6

That point was not implemented because docker can run any version and this case for server-side javascript seems too artificial. As a base case I choose node version v8.11.3 LTS.

### 3. Dockerization of the application

Created a multistage `Dockerfile` config and `docker-compose.yml` for running test cases with redis onboard. Tried to launch on `alpine` and `alpine-node`, but no luck...

### 4. Build and run locally

That was the quickest part :)

### 5. Make your application production-ready.

This was the scary part. I've removed all extra traces from source code and libraries from package json.
The libraries `babel-preset-env` and `babel-register` were left specifically in the productive environment.
I think we can drop `morgan`, `pug` and `cookie-parser` without much effort.

### 6. Deploy your docker image to AWS

I've researched some time and implement custom step for deployment bitbucket configuration, but did not test it :(

### 7. Implement CI pipeline, which automatically redeploys test application after git push to the master branch.

That wasn't done propertly. I'we create CI pipeline for lint and test application. Implemented manual case of deploiment, see previous paragraph.


## Summary


I have fun while working with this task and at least create something server-side for nodejs! The concept of horizontal scaling microservices is pretty straightforward, but the tools were too rough for the first time. Anyway, I hope you find some time to watch my code and discuss it. Cheers!
